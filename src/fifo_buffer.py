import queue
import pathlib
import threading
import numpy as np
import base_buffer


class TOTOROFIFOBuffer(base_buffer.TOTOROBuffer):
    """
    FIFO buffer, every iteration removes
    last files and appends a new one.
    """

    def data_preprocess(self, arrays: list) -> np.array:
        """
        Preprocesses flight data for future analysys.
        TODO: ARIMA approximation.
        Args:
            arrays (list of nupmy.array objects):
                List of arrays to be processed.
        """
        # arima = ARIMA()
        arima = np.zeros(12)
        return arima

    def _thread_func(self):
        """
        Main queue worker function, gets data, processes it and puts
        in a queue used in iterations.
        """
        buffer = []
        idx_buffer = []
        # load data from N-files
        for i in range(self.n_data_files):
            buffer.append(self.load_data(self.min_idx + i))
            idx_buffer.append(self.min_idx + i)
        # inject arima data
        for i in range(len(buffer)):
            if (2*i+1) >= len(buffer):
                break
            arima = self.data_preprocess([buffer[i*2],
                                          buffer[i*2+1]])
            buffer.insert(2*i+1, arima)

        cur_idx = self.min_idx + self.n_data_files
        while cur_idx <= self.max_idx:
            self.queue.put(np.concatenate(buffer, axis=None))
            tmp_arr = self.load_data(cur_idx)
            idx_buffer.append(cur_idx)
            tmp_arima = self.data_preprocess([buffer[-1], tmp_arr])
            buffer.pop(0)
            buffer.pop(0)
            buffer.append(tmp_arima)
            buffer.append(tmp_arr)
            cur_idx += 1
        self.running = False


if __name__ == "__main__":
    obj = TOTOROFIFOBuffer(
        '/run/media/moryc/HDD/interference_tests/magnetyczna/', 10)
    for item in obj:
        print(item, item.size)
    

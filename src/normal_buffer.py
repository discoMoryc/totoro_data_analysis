import scipy
import queue
import pathlib
import threading
import numpy as np
import base_buffer
import matplotlib.pyplot as plt

class TOTORONormalBuffer(base_buffer.TOTOROBuffer):
    """
    Normal buffer, stores defined number of files,
    preprocesses and returns them.
    """
    
    def __init__(self,
                 dir_path,
                 n_data_files=5,
                 queue_size=5):
        super().__init__(dir_path,n_data_files,queue_size)
        self.f = None
        self.samp_f = 2048000

    def data_preprocess(self, arrays: list) -> np.array:
        """
        Preprocesses flight data for future analysys.
        TODO: ARIMA approximation/ or not i dont fucking know 
        not my problem and i do not care anymore do what 
        you want peace
        Args:
            arrays (list of nupmy.array objects):
                List of arrays to be processed.
        """
        sxx_buf = []
        t_buf = []
        for array in arrays:
            # TU MASZ SPEKTROGRAMY SE WYBIERAJ
            f, t, sxx = scipy.signal.spectrogram(array, self.samp_f,
                                                 window='blackman',
                                                 nfft=1024,
                                                 nperseg=1024,
                                                 noverlap=512)
            sxx_buf.append(sxx)
            if t_buf:
                t += t_buf[-1][-1]
            t_buf.append(t)
            self.f = f
        return np.hstack(sxx_buf), np.concatenate(t_buf, axis=None)

    def _thread_func(self):
        """
        Main queue worker function, gets data, processes it and puts
        in a queue used in iterations.
        """
        buffer = []
        cur_idx = self.min_idx
        # load data from N-files
        while cur_idx <= self.max_idx:
            if self.n_data_files + cur_idx > self.max_idx:
                rang = self.max_idx - cur_idx + 1
            else:
                rang = self.n_data_files
            for _ in range(rang):
                buffer.append(self.load_data(cur_idx))
                cur_idx += 1
            if buffer:
                self.queue.put(self.data_preprocess(buffer))
            buffer = []
        self.running = False


if __name__ == "__main__":
    """Karol możesz to albo wziąć i tu wstawić co ma być robione
    albo zaimportować.
    Ogólnie założenie jest takie - podajesz mu ścieżkę z plikami
    (który ma tylko pliki z jednej anteny), czyli np. robisz folder
    "magnetyczna" i masz tam pliki tylko rspdx ( czy tam rsp1a już nwm).
    Automatycznie wykryje indexy i nazwe pliku.
    Jak zrobisz tak jak na dole to powinno automatycznie działac.
    Szukaj TODO to jest linia 73 kodu bodajże, tam możesz ustawić wszstko.
    związane ze spektrogramem. 
    ogólnie to jedyne co Cie obchodzi to inicjalizacja tego syfu, i obróbka danych,
    reszta funkcji nie ma dla Ciebie większego znaczenia.
    Jak chcesz wiedzieć co robi parametr w inicjalizacji, to masz w docstringu funkcji.
    pozdrawiam uprzejmie, jakby coś nie działało to dawaj znać.
    """
    obj = TOTORONormalBuffer(
        'F:\\totoro\\sdr_pliki_rsp1a\\sdrfiles\\',
        n_data_files=5,
        queue_size=1)
    for sxx, t_axis in obj:
        f = obj.f
        plt.pcolormesh(t_axis, f, sxx, norm='log')
        plt.show()

"""
Data preprocessing generator for TOTORO project.
By design, the data geneartor should be initialized
with proper parameters and later iterated through.
Proper boilerplate code for using said genrator 
will be included at a later date.
"""

import queue
import pathlib
import threading
import numpy as np


class TOTOROBuffer:
    """
    Base class for data iterators used for 
    analysys of data.
    """

    def __init__(self,
                 dir_path,
                 n_data_files=5,
                 queue_size=5):
        """
        Initialization function for DataGenerator
        Args:
            dir_path (str): Directory with 
                files to process.
            n_data_files (int, optional): Number of files
                to be included in preprocessing.
            queue_size (int, optional): Num of files to be 
                included in queue (WIP)
        """
        self.dir_path = pathlib.Path(dir_path)
        self.prefix = ''
        self.max_idx = None
        self.min_idx = None
        self.n_data_files = n_data_files
        self.set_idx()
        self.queue = queue.Queue(queue_size)
        self.running = None

    def load_data(self, idx: int) -> np.array:
        """
        Loads data from files based on index.
        Checks if file with a specific index
        exists
        Args:
            idx (int): Index of file to be loaded.
        """
        if not self.min_idx <= idx <= self.max_idx:
            raise ValueError(f'wrong index :{idx}'
                             f'-->possible idx'
                             f'range{[self.min_idx, self.max_idx]}')
        return np.array([e.real for e in np.fromfile(str(self.dir_path) + '/' +
                                                     self.prefix +
                                                     '_' + str(idx),
                                                     dtype="complex64")])

    def data_preprocess(self, arrays: list) -> np.array:
        """
        Preprocesses flight data for future analysys.
        Args:
            arrays (list of nupmy.array objects):
        """
        raise NotImplementedError

    def _thread_func(self):
        """
        Main queue worker function, gets data, processes it and puts
        in a queue used in iterations.

        IMPORTANT: Put items in self.queue and 
        when done, set self.running to False
        """
        raise NotImplementedError

    def __iter__(self):
        self.running = True
        threading.Thread(target=self._thread_func).start()
        return self

    def __next__(self):
        try:
            if not self.running and self.queue.empty():
                raise StopIteration
            q = self.queue.get(block=True, timeout=self.n_data_files * 10)
        except Exception as exc:
            raise StopIteration
        self.queue.task_done()
        return q

    def set_idx(self):
        """
        Sets max and min file index,
        as well as file prefix.
        """
        list_idx = list(self.dir_path.iterdir())
        self.prefix = list_idx[0].name.split('_')[0]

        list_idx.sort(key=lambda x: int(x.name.split('_')[1]))
        self.max_idx = int(list_idx[-1].name.split('_')[-1])
        self.min_idx = int(list_idx[0].name.split('_')[-1])

Repo for analysing data obtained from the TOTORO BEXUS33 experiment. 

Goertzel Algorithm - method for obtaining a magnitude for a single frequency in spectrum, much more calculations efficient. 

https://en.wikipedia.org/wiki/Goertzel_algorithm

https://pypi.org/project/fastgoertzel/0.1.0/

Chirp Z transform - method for obtainng a magnitude for a set of frequencies. 

https://en.wikipedia.org/wiki/Chirp_Z-transform

https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.CZT.html

Spectrogram - spectrum over time

https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html

https://en.wikipedia.org/wiki/Spectrogram

Wymagania Tomasza:
VLF: f+/- 50 Hz + tło szumowe, pasmowo 

LF: 200-300 kHz pasmowe i kilka f+/- 50 Hz + tło szumowe 

MF: 576kHz +/- 20 Hz + tło szumowe (elektryczna), 500-800 kHz pasmowe 



